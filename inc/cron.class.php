<?php

/**
 -------------------------------------------------------------------------
 Alter plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/alter
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Alter.

 Alter is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Alter is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Alter. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
*/

class PluginAlterCron {

   static function getTypeName($nb = 0) {
      return 'AlterCron';
   }

   static function cronInfo($name) {
      switch ($name) {
         case 'CloseChanges':
            return ['description' => __('Automatic changes closing', 'alter')];
      }
      return [];
   }

   static function cronCloseChanges($task = null) {

      global $DB;

      $change = new Change();
      $delay = 14;
      $condition = " `status` = '" . $change::SOLVED . "' AND `is_deleted` = 0 AND ADDDATE(`solvedate`, INTERVAL " . $delay . " DAY) < NOW()";
      $count = 0;

      foreach ($change->find($condition) as $ch) {
         $change->update([
            'id' => $ch['id'],
            'status' => $change::CLOSED,
            '_auto_update' => true,
         ]);
         $count++;
      }

      if ($count) {
         $task->addVolume($count);
         $task->log(sprintf(__('Closed %s changes', 'alter'), $count));
      }

      return 1;

   }

}
