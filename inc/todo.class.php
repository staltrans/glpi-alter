<?php

/**
 -------------------------------------------------------------------------
 Alter plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/alter
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Alter.

 Alter is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Alter is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Alter. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginAlterTodo extends CommonGLPI {

   function getTabNameForItem(CommonGLPI $item, $withtemplate = 0) {
      global $DB;
      if (!$withtemplate && $item->getType() == 'Central' && self::canAssign()) {
         if ($result = $DB->query(self::buildQuery())) {
            $count = $DB->numrows($result);
            $title = Ticket::getTypeName($count);
            if ($count > 0) {
               // поправка оступа с права
               $title .= '&nbsp;&nbsp;&nbsp;&nbsp;';
            }
            return self::createTabEntry($title, $count);
         }
      }
      return '';
   }

   static function canAssign() {
      return Session::haveRightsOr(Ticket::$rightname, [Ticket::ASSIGN, Ticket::STEAL]);
   }

   static function buildQuery() {
      $ticket_table = Ticket::getTable();
      $actor_table = Ticket_User::getTable();
      $open_statuses = join(',', [
         Ticket::INCOMING,
         Ticket::ASSIGNED,
         Ticket::PLANNED,
         Ticket::WAITING
      ]);
      $assign = CommonITILActor::ASSIGN;
      $current_user = Session::getLoginUserID();
      return "
         SELECT `t`.* FROM `$ticket_table` `t`
            LEFT JOIN `$actor_table` `tu` ON `tu`.`tickets_id` = `t`.`id`
         WHERE `t`.`is_deleted` = '0' AND `tu`.`type` = '$assign' AND `tu`.`users_id` = '$current_user' AND `t`.`status` IN ($open_statuses)
         ORDER BY `t`.`date` ASC";

   }

   static function displayTabContentForItem(CommonGLPI $item, $tabnum = 1, $withtemplate = 0) {
      global $DB;
      if ($item->getType() == 'Central' && self::canAssign()) {
         if ($result = $DB->query(self::buildQuery())) {
            $ticket = new Ticket();
            $count = $DB->numrows($result);
            echo '<table class="tab_cadrehov"><tbody>';
            echo '<tr>';
            echo '<th>' . $ticket->getTypeName($count) . '<span class="primary-bg primary-fg count">' . $count . '</span></th>';
            echo '<th>' . __('Date') . '</th>';
            echo '<th>' . __('Status') . '</th>';
            echo '<th>' . __('Priority') . '</th>';
            echo '<th>' . __('Requester') . '</th>';
            echo '<th width="50%">' . __('Description') . '</th>';
            echo '</tr>';
            while ($line = $DB->fetch_assoc($result)) {
               $ticket->getFromDB($line['id']);
               echo '<tr>';
               echo '<td>'; // title
               echo '[<a href="' . $ticket->getLinkURL() . '">' . $line['id'] .'</a>]&nbsp;';
               echo '<a href="' . $ticket->getLinkURL() . '">' . $ticket->getName() .'</a>';
               echo '</td>'; // end title
               echo '<td>'; // date
               echo Html::convDateTime($line['date']);
               echo '</td>'; // end date
               echo '<td>'; // status
               echo '<img src="' . Ticket::getStatusIconURL($line['status']) . '"';
               echo 'alt="' . Ticket::getStatus($line['status']) . '"';
               echo 'title="' . Ticket::getStatus($line['status']) . '">&nbsp;';
               echo Ticket::getSpecificValueToDisplay('status', $line['status']);
               echo '</td>'; // end status;
               echo '<td style="background:' . $_SESSION['glpipriority_' . $line['priority']] . '">'; // priority
               echo Ticket::getSpecificValueToDisplay('priority', $line['priority']);
               //$line['priority'];
               echo '</td>'; // end priority
               echo '<td>'; // requester
               foreach ($ticket->getUsers(CommonITILActor::REQUESTER) as $u) {
                  $userdata = getUserName($u['users_id'], 2);
                  echo sprintf(__('%1$s %2$s'),
                     $userdata['name'],
                     Html::showToolTip($userdata['comment'], [
                        'link' => $userdata['link'],
                        'display' => false
                     ])
                  );
                  echo '<br />';
               }
               echo '</td>'; // end requester
               echo '<td>'; // description
               echo Toolbox::unclean_html_cross_side_scripting_deep($line['content']);
               echo '</td>'; // end description
               echo '</tr>';
            }
            echo '</tbody></table>';
         }
      }
      return true;
   }

}
