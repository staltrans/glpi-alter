<?php

/**
 -------------------------------------------------------------------------
 Alter plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/alter
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Alter.

 Alter is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Alter is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Alter. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginAlterHook {

   static function preItemAdd($item) {
      $mt = new PluginAlterMailThreading($item);
      $item = $mt->addHeaders();
   }

   static function preShowItem($vals) {

      if (isset($vals['item']) && is_object($vals['item'])) {

         $type = get_class($vals['item']);

         if (($type == 'Problem' || $type == 'Change') && isset($vals['options']['tickets_id'])) {
            $t_user = new Ticket_User();
            $condition = "`tickets_id`='" . $vals['options']['tickets_id'] . "' AND `type`='" . CommonITILActor::REQUESTER . "'";
            $users = $t_user->find($condition);
            if (count($users) > 0) {
               $requester = reset($users);
               $vals['options']['_users_id_requester'] = $requester['users_id'];
            }
         }

         if ($type == 'Change' && isset($vals['options']['problems_id'])) {
            $t_user = new Problem_User();
            $condition = "`problems_id`='" . $vals['options']['problems_id'] . "' AND `type`='" . CommonITILActor::REQUESTER . "'";
            $users = $t_user->find($condition);
            if (count($users) > 0) {
               $requester = reset($users);
               $vals['options']['_users_id_requester'] = $requester['users_id'];
            }
         }

      }

   }

}
