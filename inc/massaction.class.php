<?php

/**
 -------------------------------------------------------------------------
 Alter plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/alter
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Alter.

 Alter is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Alter is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Alter. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginAlterMassaction {

   static function showMassiveActionsSubForm(MassiveAction $ma) {
      switch ($ma->getAction()) {
         case 'Copy':
            echo Html::submit(__('Copy'), ['name' => 'copy']);
            return true;
      }
   }

   static function processMassiveActionsForOneItemtype(MassiveAction $ma, CommonDBTM $item, array $ids) {
      global $DB;

      switch ($ma->getAction()) {
         case 'Copy' :
            if ($item->getType() == 'Profile') {
               if (Profile::canCreate()) {
                  Session::addMessageAfterRedirect(__('New profiles', 'alter') . ':');
                  foreach ($ids as $id) {
                     if ($item->getFromDB($id)) {
                        $rights = ProfileRight::getProfileRights($id);
                        $input = $item->fields;
                        unset($input['id']);
                        $input['name'] = $input['name'] . ' ' . _x('item', 'copy', 'alter');
                        if ($new_id = $item->add($input)) {
                           ProfileRight::updateProfileRights($new_id, $rights);
                           Session::addMessageAfterRedirect($item->getLink());
                           $ma->itemDone($item->getType(), $id, MassiveAction::ACTION_OK);
                        } else {
                           $ma->itemDone($item->getType(), $id, MassiveAction::ACTION_KO);
                        }
                     } else {
                        $ma->itemDone($item->getType(), $id, MassiveAction::ACTION_KO);
                     }
                  }
               } else {
                  Session::addMessageAfterRedirect(__('Access denied'), false, ERROR);
                  $ma->itemDone($item->getType(), $id, MassiveAction::ACTION_KO);
               }
            } else {
               $ma->itemDone($item->getType(), $ids, MassiveAction::ACTION_KO);
            }
            return;
      }
   }
}
