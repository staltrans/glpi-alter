<?php

/**
 -------------------------------------------------------------------------
 Alter plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/alter
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Alter.

 Alter is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Alter is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Alter. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginAlterMailThreading {

   private $qm;

   function __construct(QueuedMail $item) {
      $this->qm = $item;
   }

   function thread($order = '', $limit = '') {
      if (isset($this->qm->input['itemtype']) && isset($this->qm->input['items_id']) && isset($this->qm->input['recipient'])) {
         $condition = "`itemtype`='" . $this->qm->input['itemtype'] .
            "' AND `items_id`='" . $this->qm->input['items_id'] .
            "' AND `recipient`='" . $this->qm->input['recipient'] . "'";
         return $this->qm->find($condition, $order, $limit);
      }
      return null;
   }

   function firts() {
      return $this->thread("`id` ASC", 1);
   }

   function last() {
      return $this->thread("`id` DESC", 1);
   }

   function addHeaders() {
      $this->qm->input['headers']['Message-ID'] = $this->buildMessageID();
      $references = [];
      foreach ($this->thread() as $msg) {
         $headers = $this->decodeHeaders($msg['headers']);
         if (isset($headers['Message-ID'])) {
            $references[] = $headers['Message-ID'];
         }
      }
      $references[] = $this->qm->input['headers']['Message-ID'];
      $this->qm->input['headers']['References'] = join(' ', $references);
      return $this->qm;
   }

   private function buildMessageID() {
      return '<' . md5(join('-', $this->qm->input)) . '-' . $this->qm->input['sender'] . '>';
   }

   private function decodeHeaders($headers) {
      return json_decode($headers, true);
   }

}
