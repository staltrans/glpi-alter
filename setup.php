<?php
/*
 -------------------------------------------------------------------------
 Alter plugin for GLPI
 Copyright (C) 2017 by the Alter Development Team.

 https://bitbucket.org/staltrans/alter
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Alter.

 Alter is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Alter is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Alter. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

define('PLUGIN_ALTER_VERSION', '0.6');

/**
 * Init hooks of the plugin.
 * REQUIRED
 *
 * @return void
 */
function plugin_init_alter() {
   global $PLUGIN_HOOKS;

   Plugin::registerClass('PluginAlterTodo', ['addtabon' => 'Central']);

   $PLUGIN_HOOKS['csrf_compliant']['alter'] = true;
   $PLUGIN_HOOKS['use_massive_action']['alter'] = true;

   $PLUGIN_HOOKS['pre_item_add']['alter'] = [
      'QueuedMail' => ['PluginAlterHook', 'preItemAdd'],
   ];

   $PLUGIN_HOOKS['pre_show_item']['alter'] = ['PluginAlterHook', 'preShowItem'];

}

/**
 * Get the name and the version of the plugin
 * REQUIRED
 *
 * @return array
 */
function plugin_version_alter() {
   return [
      'name'           => 'Alter',
      'version'        => PLUGIN_ALTER_VERSION,
      'author'         => '<a href="https://bitbucket.org/staltrans/">StalTrans</a>',
      'license'        => 'GPLv3',
      'homepage'       => 'https://bitbucket.org/staltrans/glpi-alter',
      'minGlpiVersion' => '9.1'
   ];
}

/**
 * Check pre-requisites before install
 * OPTIONNAL, but recommanded
 *
 * @return boolean
 */
function plugin_alter_check_prerequisites() {
   // Strict version check (could be less strict, or could allow various version)
   if (version_compare(GLPI_VERSION, '9.1', 'lt')) {
      if (method_exists('Plugin', 'messageIncompatible')) {
         echo Plugin::messageIncompatible('core', '9.1');
      } else {
         echo "This plugin requires GLPI >= 9.1";
      }
      return false;
   }
   return true;
}

/**
 * Check configuration process
 *
 * @param boolean $verbose Whether to display message on failure. Defaults to false
 *
 * @return boolean
 */
function plugin_alter_check_config($verbose = false) {
   if (true) { // Your configuration check
      return true;
   }

   if ($verbose) {
      _e('Installed / not configured', 'alter');
   }
   return false;
}
