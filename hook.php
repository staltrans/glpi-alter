<?php
/*
 -------------------------------------------------------------------------
 Alter plugin for GLPI
 Copyright (C) 2017 by the Alter Development Team.

 https://bitbucket.org/staltrans/alter
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Alter.

 Alter is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Alter is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Alter. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

/**
 * Plugin install process
 *
 * @return boolean
 */
function plugin_alter_install() {

   $taskopt = [
      'allowmode' => CronTask::MODE_EXTERNAL,
      'mode'      => CronTask::MODE_EXTERNAL,
   ];

   CronTask::Unregister('Alter');
   CronTask::Register('PluginAlterCron', 'CloseChanges', DAY_TIMESTAMP, $taskopt);

   return true;
}

/**
 * Plugin uninstall process
 *
 * @return boolean
 */
function plugin_alter_uninstall() {

   CronTask::Unregister('Alter');
   CronTask::Unregister('CloseChanges');

   return true;
}

function plugin_alter_MassiveActions($type) {
   switch ($type) {
      case 'Profile' :
         return ['PluginAlterMassaction' .
            MassiveAction::CLASS_ACTION_SEPARATOR .
            'Copy' => __('Copy a Profile', 'alter')];
   }

   return [];
}
