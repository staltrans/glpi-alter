# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-04-04 10:36+0500\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: hook.php:34
msgid "Copy a Profile"
msgstr ""

#: setup.php:67
msgid "Installed / not configured"
msgstr ""

#: inc/cron.class.php:12
msgid "Auto close tickets"
msgstr ""

#: inc/massaction.class.php:20
msgid "New profiles"
msgstr ""

#: inc/massaction.class.php:26
msgctxt "item"
msgid "copy"
msgstr ""
